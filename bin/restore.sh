#!/usr/bin/env sh
#
# Simple hack script to restore __revision__, __COPYRIGHT_, 3.1.2
# and other similar variables to what gets checked in to source.  This
# comes in handy when people send in diffs based on the released source.
#

if test "X$*" = "X"; then
    DIRS="src test"
else
    DIRS="$*"
fi

SEPARATOR="================================================================================"

header() {
    arg_space="$1 "
    dots=`echo "$arg_space" | sed 's/./\./g'`
    echo "$SEPARATOR" | sed "s;$dots;$arg_space;"
}

for i in `find $DIRS -name '*.py'`; do
    header $i
    ed $i <<EOF
g/Copyright (c) 2001.*SCons Foundation/s//Copyright (c) 2001 - 2019 The SCons Foundation/p
w
/^__revision__ = /s/= .*/= "bin/restore.sh bee7caf9defd6e108fc2998a2520ddb36a967691 2019-12-17 02:07:09 bdeegan"/p
w
q
EOF
done

for i in `find $DIRS -name 'scons.bat'`; do
    header $i
    ed $i <<EOF
g/Copyright (c) 2001.*SCons Foundation/s//Copyright (c) 2001 - 2019 The SCons Foundation/p
w
/^@REM src\/script\/scons.bat/s/@REM .* knight/@REM bin/restore.sh bee7caf9defd6e108fc2998a2520ddb36a967691 2019-12-17 02:07:09 bdeegan/p
w
q
EOF
done

for i in `find $DIRS -name '__init__.py' -o -name 'scons.py' -o -name 'sconsign.py'`; do
    header $i
    ed $i <<EOF
/^__version__ = /s/= .*/= "3.1.2"/p
w
/^__build__ = /s/= .*/= "bee7caf9defd6e108fc2998a2520ddb36a967691"/p
w
/^__buildsys__ = /s/= .*/= "octodog"/p
w
/^__date__ = /s/= .*/= "2019-12-17 02:07:09"/p
w
/^__developer__ = /s/= .*/= "bdeegan"/p
w
q
EOF
done

for i in `find $DIRS -name 'setup.py'`; do
    header $i
    ed $i <<EOF
/^ *version = /s/= .*/= "3.1.2",/p
w
q
EOF
done

for i in `find $DIRS -name '*.txt'`; do
    header $i
    ed $i <<EOF
g/Copyright (c) 2001.*SCons Foundation/s//Copyright (c) 2001 - 2019 The SCons Foundation/p
w
/# [^ ]* 0.96.[CD][0-9]* [0-9\/]* [0-9:]* knight$/s/.*/# bin/restore.sh bee7caf9defd6e108fc2998a2520ddb36a967691 2019-12-17 02:07:09 bdeegan/p
w
/Version [0-9][0-9]*\.[0-9][0-9]*/s//Version 3.1.2/p
w
q
EOF
done

for i in `find $DIRS -name '*.xml'`; do
    header $i
    ed $i <<EOF
g/Copyright (c) 2001.*SCons Foundation/s//Copyright (c) 2001 - 2019 The SCons Foundation/p
w
q
EOF
done
